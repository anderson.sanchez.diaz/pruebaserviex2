﻿using DataAcces;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFServiceServiex
{
    // NOTA: puede usar el comando "Rename" del menú "Refactorizar" para cambiar el nombre de clase "Service1" en el código, en svc y en el archivo de configuración.
    // NOTE: para iniciar el Cliente de prueba WCF para probar este servicio, seleccione Service1.svc o Service1.svc.cs en el Explorador de soluciones e inicie la depuración.
    public class ServiceData : IServiceData
    {
        public int SetUsuario(Usuario us)
        {
            using (var BDatos = new AppDataContext())
            {
                SqlParameter[] parametros = { new SqlParameter("Nombre", us.Nombre),
                new SqlParameter("FechaNacimiento", us.FechaNac),
                new SqlParameter("Sexo", us.Sexo)};

                BDatos.Database.ExecuteSqlCommand("[dbo].[AgregarUsuario] @Nombre, @FechaNacimiento, @Sexo", parametros);

                //BDatos.Usuarios.Add(new DataAcces.Usuario { 
                //    Nombre=us.Nombre,
                //    FechaNacimiento = us.FechaNac,
                //    Sexo=us.Sexo
                //});
                //return BDatos.SaveChanges();
                return 0;
            }

        }

        public List<Usuario> GetUsuarios()
        {
            using (var BDatos = new AppDataContext())
            {
                return BDatos.Usuarios.Select((f) => new Usuario
                {
                    Id = f.Id,
                    Nombre = f.Nombre,
                    FechaNac = f.FechaNacimiento,
                    Sexo = f.Sexo
                }).ToList();
            }

        }

        public int EliminarUsuario(int IdUser)
        {
            using (var BDatos = new AppDataContext())
            {
                BDatos.Database.ExecuteSqlCommand("[dbo].[EliminarUsuario] @Id", new SqlParameter("Id", IdUser));
            }
            return 0;
        }

        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }
    }
}
