﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataAcces
{
    public class AppDataContext : DbContext
    {
        public AppDataContext() : base("Data Source=72.167.36.118,1433;Initial Catalog=Servicex;User=UserServiex;password=UserServiex;Connection Timeout=200; pooling=true;MultipleActiveResultSets=True")
        {

        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Usuario> Usuarios { get; set; }

    }

    public class Usuario
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public DateTime FechaNacimiento { get; set; }
        public string Sexo { get; set; }
    }
}
