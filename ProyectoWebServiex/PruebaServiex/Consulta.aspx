﻿<%@ Page Title="Consulta" Language="VB" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="~/Consulta.aspx.vb" Inherits="PruebaServiex.Consulta" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">


    <p style="font-size: large">
        <strong>Consulta de Usuarios</strong></p>
    <p style="font-size: small">
        &nbsp;</p>
    <p style="font-size: large">
        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" DataKeyNames="Id">
            <Columns>
                <asp:CommandField ShowDeleteButton="True" />
                <asp:BoundField DataField="Id" HeaderText="Id" SortExpression="Id" />
                <asp:BoundField DataField="Nombre" HeaderText="Nombre" SortExpression="Nombre" />
                <asp:BoundField DataField="FechaNac" HeaderText="FechaNac" SortExpression="FechaNac" />
                <asp:BoundField DataField="Sexo" HeaderText="Sexo" SortExpression="Sexo" />
            </Columns>
        </asp:GridView>
        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" DeleteMethod="EliminarUsuario" SelectMethod="GetUsuarios" TypeName="PruebaServiex.WCFServiex.ServiceDataClient">
            <DeleteParameters>
                <asp:Parameter Name="Id" Type="Int32" />
            </DeleteParameters>
        </asp:ObjectDataSource>
    </p>


</asp:Content>