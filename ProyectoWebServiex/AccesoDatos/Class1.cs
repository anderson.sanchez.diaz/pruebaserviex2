﻿using System;
using System.Data.Entity;

namespace AccesoDatos
{
    public class AppDataContext : DbContext
    {
        public AppDataContext():base("") { 
        
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
        }

        public virtual DbSet<Usuario> Usuarios { get; set; }

    }

    public class Usuario {
    public string Nombre { get; set; }
    public DateTime FechaNac { get; set; }
    public string Sexo { get; set; }
    }





}
